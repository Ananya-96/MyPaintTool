var a,b,tempcxt,choosenColor;
function elt(name, attributes) {
  var node = document.createElement(name);
  if (attributes) {
    for (var attr in attributes)
      if (attributes.hasOwnProperty(attr))
        node.setAttribute(attr, attributes[attr]);
  }
  for (var i = 2; i < arguments.length; i++) {
    var child = arguments[i];
    if (typeof child == "string")
      child = document.createTextNode(child);

    node.appendChild(child);

  }

  return node;
}

function loadImageURL(cx, url) {
  var image = document.createElement("img");
  image.addEventListener("load", function() {

    cx.drawImage(image, 0, 0);

  });
  image.src = url;
}
function trackDrag(onMove, onEnd) {
  function end(event) {
    removeEventListener("mousemove", onMove);
    removeEventListener("mouseup", end);
    if (onEnd)
      onEnd(event);
  }
  addEventListener("mousemove", onMove);
  addEventListener("mouseup", end);
}
function relativePos(event,element) {

 var rect = element.getBoundingClientRect();
  return {x: Math.floor(event.clientX - rect.left),
          y: Math.floor(event.clientY - rect.top)};
}
function randomPointInRadius(radius) {
  for (;;) {
    var x = Math.random() * 2 - 1;
    var y = Math.random() * 2 - 1;
    if (x * x + y * y <= 1)
      return {x: x * radius, y: y * radius};
  }
}

var tools = Object.create(null);
var controls = Object.create(null);
controls.brushSize = function(cx,tempcxt) {
  var select = elt("input",{id:"number",type:"number",min:"1",max:"70",style:"width:48px;height:39px;font-size:25px;",value:"1",autofocus:"true"});

  select.addEventListener("change", function() {
    if (select.value==0) {
      window.alert("Size cannot be 0");
    }
      tempcxt.lineWidth = select.value;

    });
    return elt("div", {style:"margin:0px auto;"}, select);


};

tools.Line = function(event, cx, tempcxt, onEnd) {
  tempcxt.lineCap = "round";
  var pos = relativePos(event, cx.canvas);
  trackDrag(function(event) {
    tempcxt.beginPath();
    tempcxt.moveTo(pos.x, pos.y);
    pos = relativePos(event, cx.canvas);
    tempcxt.lineTo(pos.x, pos.y);
    tempcxt.stroke();
  }, onEnd);
};
tools.Rectangle = function(event,cx,tempcxt) {


  var    w = tempcanvas.width,
      h = tempcanvas.height;
  cx.translate(0.5, 0.5);
  var pos=relativePos(event,tempcxt.canvas);

    var  isDown = true;

  canvas.onmouseup = function() {
      isDown = false;

      tempcxt.drawImage(canvas, 0, 0);
      cx.clearRect(0, 0, w, h);
  }
     canvas.onmousemove = function(e) {

      if (!isDown) return;

      var rect = tempcanvas.getBoundingClientRect(),
          x2 = e.clientX - rect.left,
          y2 = e.clientY - rect.top;

      cx.clearRect(0, 0, w, h);
      drawRect(pos.x, pos.y, x2, y2);



  }

  function drawRect(x1, y1, x2, y2) {


cx.lineWidth=tempcxt.lineWidth;
cx.fillStyle=tempcxt.fillStyle;
cx.strokeStyle=tempcxt.strokeStyle;
      cx.beginPath();
      cx.moveTo(x1,y1);
      leftMost=Math.min(x1,x2);
      topMost=Math.min(y1,y2);
      width=Math.abs(x1-x2);
      height=Math.abs(y1-y2);
      cx.rect(leftMost,topMost,width,height);
      cx.stroke();
  }

  };
  tools.Ellipse = function(event,cx,tempcxt) {


    var    w = tempcanvas.width,
        h = tempcanvas.height;

    cx.translate(0.5, 0.5);



        var pos=relativePos(event,tempcxt.canvas);

      var  isDown = true;

    canvas.onmouseup = function() {
        isDown = false;

        tempcxt.drawImage(canvas, 0, 0);
        cx.clearRect(0, 0, w, h);
    }
   canvas.onmousemove = function(e) {

        if (!isDown) return;

        var rect = tempcanvas.getBoundingClientRect(),
            x2 = e.clientX - rect.left,
            y2 = e.clientY - rect.top;

        cx.clearRect(0, 0, w, h);
        drawEllipse(pos.x, pos.y, x2, y2);



    }

    function drawEllipse(x1, y1, x2, y2) {



      var radiusX = (x2 - x1) * 0.5,
          radiusY = (y2 - y1) * 0.5,
          centerX = x1 + radiusX,
          centerY = y1 + radiusY,
          step = 0.01,
          a = step,
          pi2 = Math.PI * 2 - step;
cx.lineWidth=tempcxt.lineWidth;
cx.fillStyle=tempcxt.fillStyle;
cx.strokeStyle=tempcxt.strokeStyle;

      cx.beginPath();
      cx.moveTo(centerX + radiusX * Math.cos(0),
                 centerY + radiusY * Math.sin(0));

      for(; a < pi2; a += step) {
          cx.lineTo(centerX + radiusX * Math.cos(a),
                     centerY + radiusY * Math.sin(a));
      }

      cx.closePath();
        cx.stroke();
    }

    };
    tools.Spray = function(event, cx,tempcxt) {
      var radius = tempcxt.lineWidth / 2;
      var area = radius * radius * Math.PI;
      var dotsPerTick = Math.ceil(area / 30);

      var currentPos = relativePos(event, cx.canvas);

      var spray = setInterval(function() {
        for (var i = 0; i < dotsPerTick; i++) {
          var offset = randomPointInRadius(radius);
          tempcxt.fillRect(currentPos.x + offset.x,
                      currentPos.y + offset.y, 1, 1);
        }
      }, 25);
      trackDrag(function(event) {
        currentPos = relativePos(event, cx.canvas);
      }, function() {
        clearInterval(spray);
      });
    };
    tools.Text = function(event, cx,tempcxt) {
      var text = prompt("Text:", "");
      if (text) {
        var pos = relativePos(event, cx.canvas);
        tempcxt.font = Math.max(7, tempcxt.lineWidth) + "px sans-serif";
        tempcxt.fillText(text, pos.x, pos.y);
      }
    };
    tools.Erase = function(event, cx,tempcxt) {
      tempcxt.globalCompositeOperation = "destination-out";
      tools.Line(event, cx, tempcxt,function() {
      tempcxt.globalCompositeOperation = "source-over";
      });
    };
tools.Fill=function(event,cx,temcxt) {
if (choosenColor) {
  temcxt.fillStyle=choosenColor;
}
else {
  temcxt.fillStyle='#000000';
}
tempcxt.fillRect(0, 0, canvas.width, canvas.height);

}
function line_set() {
  a="Line";
}
function rectangle_set() {
  a="Rectangle";
}
function ellipse_set() {
  a="Ellipse";
}
function spray_set() {
  a="Spray";
}
function text_set() {
  a="Text";
}
function erase_set() {
  a="Erase";
}
function fill_set() {
  a="Fill";
}
controls.tool = function(cx,tempcxt) {

  var div = elt("div",{id:"tools-div"});
  for (var name in tools)
  {
    if(name=="Line")
    {
      var button=elt("button",{onclick:"line_set()"},elt("i",{class:"material-icons",style:"font-size:36px;"},"edit"));
  div.appendChild(button);
  var br=elt("br");
  div.appendChild(br);

    }
    if(name=="Rectangle"){
      var button1=elt("button",{onclick:"rectangle_set()"},elt("i",{class:"material-icons",style:"font-size:36px"},"crop_16_9"));
    div.appendChild(button1);
  var br=elt("br");
div.appendChild(br);
  }
  if(name=="Ellipse"){
    var button2=elt("button",{onclick:"ellipse_set()"},elt("i",{class:"material-icons",style:"font-size:36px"},"panorama_fish_eye"));
  div.appendChild(button2);
  var br=elt("br");
  div.appendChild(br);

}
if(name=="Spray"){
  var button3=elt("button",{onclick:"spray_set()"},elt("i",{class:"material-icons",style:"font-size:36px"},"blur_on"));

div.appendChild(button3);
var br=elt("br");
div.appendChild(br);
}
if(name=="Text"){
  var button4=elt("button",{onclick:"text_set()"},elt("i",{class:"material-icons",style:"font-size:36px"},"font_download"));
div.appendChild(button4);
var br=elt("br");
div.appendChild(br);

}
if(name=="Erase"){
  var button5=elt("button",{onclick:"erase_set()"},elt("i",{class:"material-icons",style:"font-size:36px"},"clear"));
div.appendChild(button5);
  var br=elt("br");
div.appendChild(br);
}
if(name=="Fill"){
  var button8=elt("button",{onclick:"fill_set()"},elt("i",{class:"material-icons",style:"font-size:36px"},"format_color_fill"));
  div.appendChild(button8);
  var br=elt("br");
  div.appendChild(br);

}
}
  cx.canvas.addEventListener("mousedown", function(event) {
    if (event.which == 1) {

      tools[a](event, cx,tempcxt);
      event.preventDefault();
    }

  });


  return elt("div", {class:"tool-class", style:"margin:0px auto;"}, div);
};
function delete_set() {
  tempcxt.fillStyle='#ffffff';
  tempcxt.fillRect(0,0,canvas.width,canvas.height);
}
controls.delete = function(cx,tempcxt) {
  button9=elt("button",{onclick:"delete_set()"},elt("i",{class:"material-icons",style:"font-size:36px"},"delete"));
  return elt("div", {style:"width:52px;margin:0px auto;"}, button9);
};


controls.color = function(cx,tempcxt) {
  var input = elt("input", {type: "color",style:"height:41px;width:46px;"});
  input.addEventListener("change", function() {
choosenColor=input.value;
    tempcxt.fillStyle = input.value;
    tempcxt.strokeStyle = input.value;
  });
  return elt("div", {style:"width:52px;margin:0px auto;"}, input);
};


controls.openFile = function(cx,tempcxt) {
  var div=elt("div");
  var button6=elt("button", null,elt("label",{for:"file-input"},elt("i",{class:"material-icons",style:"font-size:36px"},"add_a_photo")));

  div.appendChild(button6);

  var input = elt("input", {id:"file-input",type: "file", style:"display:none;"});
  div.appendChild(input);
  input.addEventListener("change", function() {
    if (input.files.length == 0) return;
    var reader = new FileReader();
    reader.addEventListener("load", function() {
      loadImageURL(tempcxt, reader.result);
    });
    reader.readAsDataURL(input.files[0]);
  });
  return elt("div",  {style:"margin:0px auto;"}, div);
};
function link_set() {
  b=window.prompt("Enter url");
  if(b)
{    loadImageURL(tempcxt,b);
 }
}
controls.openURL = function(cx,tempcxt) {
  var input = elt("button", {onclick:"link_set()"}, elt("i",{class:"material-icons",style:"font-size:36px"},"link"));

  return elt("div", {style:"margin:0px auto;"}, input);
};
controls.save = function(cx,tempcxt) {
 var div=elt("div");
var button7=elt("button");
  var link =  elt("a", {href: "/",id:"save"}, elt("i",{class:"material-icons",style:"font-size:36px"},"file_download"));
   button7.appendChild(link);
div.appendChild(button7)
  function update() {
    try {
      link.href = tempcxt.canvas.toDataURL();
    } catch (e) {
      if (e instanceof SecurityError)
        link.href = "javascript:alert(" +
          JSON.stringify("Can't save: " + e.toString()) + ")";
      else
        throw e;
    }
  }
  link.addEventListener("mouseover", update);
  link.addEventListener("focus", update);
  return elt("div",{style:"margin:0px auto;"}, div);
};

function createPaint(parent) {
  var tempcanvas = elt("canvas", {id:"tempcanvas", width:"1120", height:"580", style:"position: absolute; margin-left:130px; border:2px solid black"});
   tempcxt = tempcanvas.getContext('2d');
  var canvas = elt("canvas", {id:"canvas", width:"1120", height:"580", style:"position: absolute; margin-left:130px;  border:2px solid black"});
  var cx = canvas.getContext("2d");



  var toolbar = elt("div", {class: "toolbar demo-card-wide mdl-card", id: "toolbar"});
  for (var name in controls)
    toolbar.appendChild(controls[name](cx,tempcxt));

  var panel = elt("div", {class: "picturepanel"}, tempcanvas, canvas);

  parent.appendChild(elt("div", null, panel, toolbar));
}
createPaint(document.body);
