# PAINT TOOL

The tool uses javascript which allows the user to play with various tools and draw on a HTML5 Canvas. The tools include various shapes like rectangle, ellipse and supports free-hand drawing too. It also has options for changing the width and color of the drawing tool.

# A brief explanation

The tool basically consists of a picturepanel and a toolbar.

# picturepanel

It contains the 2 canvases :

1. canvas
2. tempcanvas

# toolbar

The various options provided by the paintTool is pushed into a controls array which is further classified into:

1. size:

          This requires the user to enter the desired size for the available tools. It ranges from 1px to 70px.

2. tools:

          The tools array have several elements:

             1. line  -  to create a free-hand drawing  
             2. rectangle - to draw a rectangle
             3. ellipse - to draw an ellipse
             4. spray - to spray the chosen color in different widths
             5. text - to insert user-defined text
             6. erase - erase the required area
             7. fill - fill the background of the canvas

3.  delete:

          This option is used to clear the entire canvas.

4. color:

          The color option allows the user to select the color for the   various available tools according to their choice.

5. add a photo:          

          The add a photo option asks for the location of the image on the pc to be given by the user. It inserts the specified image onto the canvas.

6. add a url:

          This feature inserts the image from the web when the URL of this image is given.

7. preview:

          This allows a preview of the canvas which can be downloaded  further by right clicking on it.   


# Design

The tools are designed using material design icons which gives a better look and feel to the paint tool. The material design icons are available in the website https://material.io/icons/ . The icons are wrapped into buttons of the same size 36px to give a neat look.


# Constraints

Basically, the main constraint of this tool was to show the rectangle or ellipse during the dragging, without actually drawing to the canvas until the mouse button is released. The 2 main functions were:

          1. To find the leftMost and topMost co-ordinate which gives us the initial point that indicates the point from which the rectangle should be drawn.

          2. Allow the user to drag over the canvas to any size and when the mouseup is fired, draw the rectangle on the point where the event was fired.

# Solution

A temporary canvas was created to give this effect and push the image into the main canvas every time the user drags the mouse. Finally when the mouse button is released the program gets the position of the point where the mouseup was fired and draws the rectangle or ellipse on the temporary canvas.

# Links included

[To get the material design card ] ("https://code.getmdl.io/1.3.0/material.indigo-pink.min.css")

[ To get the material design icons] ("https://fonts.googleapis.com/icon?family=Material+Icons")

# Include the javascript file -> paint.js

Declare the variables a, b, tempcxt, choosenColor

# function elt(name,attributes)
# Use - To create a node with the specified type and attributes.
Since the program involves lots of tags, we construct a function to create the elements dynamically using JavaScript. The createElement() is a pre-defined function which returns a node of the specified type.

Syntax: var variable_name = document.createElement(type);

The node can also have several attributes like class, id, style. If the specified attribute has its own property, for eg:

<div style="width:20px">

Here, style is an attribute of div while it has its own attribute width.
A for loop is run to check if the attributes have a property using hasOwnProperty() and append those properties with the setAttribute().

If the argument is of type "string" then a text node is created using createTextNode(child) and finally appended to the node.

For eg:
<div style="width:20px">Hi </div>

is written as

elt("div",{style:"width:20px"},"Hi");

The function returns the node.

# function loadImageURL(cx,url)

# Use - To load the image on to the canvas

 The image source is specified by the URL. The image tag is created using document.createElement("img") which listens for a load event. It is done by:
 image.addEventListener("load",function(){});

Here cx refers to the context of the canvas which is used to draw the image on the canvas. The function drawImage draws a image or video onto the canvas at the specified x and y co-ordinate position.

# function trackDrag(onMove,onEnd)

# Use - To check if the mouse is dragged onto the keyboard

This function listens for a mousemove and a mouseup event. It takes two parameters onMove and onEnd.
onMove is the function which should be done on mousemove.
When mouseup is fired the function end() is called which removes the event listener and ends the event.

# function relativePos(event, element)

# Use - To find the position of the mouse pointer

Since the program involves many tools which requires the mouse pointer location we define a function which calculates the exact co-ordinates of the mouse pointer on the given element. Here the element is always the context of the canvas.

The method getBoundingClientRect()  returns the size of an element and its position relative to the viewport. rect is a DOMRect object with eight properties: left, top, right, bottom, x, y, width, height. clientX and clientY returns the x and y co-ordinates with respect to the window size which is the client area. But since we need the mouse pointer location with respect to the canvas area, the rect.left and rect.right are subtracted from the clientX and clientY. The math.floor() is used to round the co-ordinates to its nearest integer.

# function randomPointInRadius(radius)

It runs a infinite for loop which uses Math.random() to generate the points with the given radius.

# controls.brushSize

A input tag with type number is created to get the size of the brush from the user. A addEventListener() method is added to the tag which listens for change event on which the tempcxt.lineWidth is set to the specified value. The input tag is finally added to a <div> element and the node is returned.

# tools.Line

Before the line is drawn onto the canvas, lineCap is set to round which adds a rounded end cap to each end of a line. The default value is "butt". Calculate the position of the mouse pointer using the relativePos(). beginPath() is used to begin the actual path of the line to be drawn. Move the pointer to the calculated position and re-calculate the position. The stroke() is used to draw the line onto the canvas.

# tools.Rectangle

The top left corner of the canvas has the co-ordinates (0,0). translate() method is used to shift the x and y axis of the canvas. Set a variable isDown as true when the mouse is held down and the starting point of the rectangle is calculated. On mouseup isDown is set to false and the canvas is cleared. On mousemove and when isDown is true, the position of the opposite vertex is calculated and drawRect() is called. drawRect() calculates the top left vertex, width and height of the rectangle to be drawn and cx.rect() is used to perform the function of creating the rectangle.

# tools.ellipse

The steps are similar to that of the rectangle except for the actual calculation of radius and center.    


# tools.spray

The radius is calculated according to the lineWidth and dotsPerTick denotes the maximum dots along the line. The interval is set to 25ms and the rectangle is filled with the dots using a for loop. The offset.x and offset.y gives the position of the mouse pointer relative
to the document.

# tools.text

On mousedown, a prompt box is used to get the input from the user which is placed at the position of mouse click.

# tools.erase

The globalCompositeOperation property sets or returns how a source (new) image are drawn onto a destination (existing) image.
source image = drawings you are about to place onto the canvas.
destination image = drawings that are already placed onto the canvas.
Destination-out	is used to display the destination image out of the source image. Only the part of the destination image that is OUTSIDE the source image is shown, and the source image is transparent. Source-over	displays the source image over the destination image.

# tools.fill

This feature is used to fill the background colour of the canvas. On mousedown, the background is filled with the chosen colour and if the colour is not selected the default colour, black is used to colour the background.

# controls.tool

This consists of a div element which encapsulates all the tools. Each tool is defined inside a button tag which is represented using material-design icons. The buttons have a onclick attribute which calls a function that sets the tool to be executed. The function finally returns a div node.

# controls.color

The color feature is used to specify the color to be used for all the available tools. An input tag of the type color is used to get the color from the user. When the color is chosen, the fillStyle and strokeStyle is set to the vaue of the input box.

# controls.openFile

This feature is used to insert an image from the pc. An input tag of the type file is used for file input. When the file is selected, FileReader is used to read the contents of a Blob or File. The reader listens for load event and when the load event is fired the loadImageURL(context,reader) is called to load the image onto the canvas. The readAsDataURL method is used to read the contents of the specified Blob or File. When the read operation is finished, the readyState becomes DONE, and the loadend is triggered. At that time, the result attribute contains  the data as a URL representing the file's data as a base64 encoded string.

# controls.openURL

On mousedown, window.prompt is fired to get the url from the user. When the url is specified the onclick function calls the loadImageURL function with the tempcxt and the specified url as the parameters.

# controls.save

This feature allows a preview of the image drawn on the canvas. A hyperlink is created which points to the toDataURL attribute of the canvas. The HTMLCanvasElement.toDataURL() method returns a data URI containing a representation of the image in the format specified by the type parameter which by default is in PNG. The returned image is in a resolution of 96 dpi. When the toDataURL returns null a alert box is used to warn the user.

# createPaint(parent)

This function is used to create the canvas and also invokes the rest of the functions. It creates 2 div tags with the class name picturepanel and toolbar. The picturepanel contains both the canvases and toolbar contains the controls and tools. Finally it is appended to the body of the html page.
